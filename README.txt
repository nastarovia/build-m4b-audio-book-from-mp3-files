This project is designed to create an audio book from several MP3 files

Input are all MP3 files in a directory, output is a single .m4b file. 

Assumptions:
   file names contain the track number and the track number is the first numeric part of the filename. Leading zeroes are not required
      examples for valid names are:
         "title001.mp3" or "001title.mp3" or "album001title.mp3" or "artist - 1 - title 4711.mp3"
         in all cases the track number will be identified as 1
   cover can be either extracted from FIRST mp3 file (sorted by track) or a .jpg/.png file named "folder" or "cover" can be used. if exactly one picture is in the input folder this can be used
      Preferences are as follows:
         1. look for cover.png
         2. look for cover.jpg
         3. look for folder.png
         4. look for folder.jpg
         5. look for any .jpg file, if only one is found this will be taken
         6. look for any .jpg file, if only one is found this will be taken
         7. try to extract the cover from first .mp3 file
         8. no cover will be embedded
   MP3 Metadata can be used to get artist / album etc. Inaccurate ID3 tags may be a problem. Maybe the input directory name can be used???
   where to place the output file
   GUI or commandline?
   3rd Party tools (included in subdirectory Utils)
      ffmpeg
      kid3-cli
      
   
   