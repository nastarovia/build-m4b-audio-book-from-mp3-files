# Build M4B audio book from MP3 files

The mp4 format is a good choice to handle audio book e.g. on Apple devices. 
This project will publish scripts to build such .m4b files from mp3 input.
Features included are chaptering, maintaining Metadata and include an optional cover

Currently the scripts are tested but need more testing.

Documentation and installation instructions will follow soon.