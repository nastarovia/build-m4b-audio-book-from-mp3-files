ffmpeg and kid3 are included, no modification to the software was done.

ffmpeg can be found at https://www.ffmpeg.org/
ffmgeg was downloaded from https://ffmpeg.zeranoe.com/builds/win64/static/ as 64-bit binary.
the included build is ffmpeg-20200324-e5d25d1-win64-static.zip

kid3 can be found at https://kid3.kde.org/ 
kid3 was downloaded from https://kid3.kde.org/#download as 64-bit binary.
the included build is kid3-3.8.2-win32-x64.zip

If you need other builds, pls. visit the download sites
